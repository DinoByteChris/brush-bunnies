﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderBunny_Controller : MonoBehaviour {

	public float moveSpeed;
	public float jump;
	public Rigidbody2D rb;
	public AudioClip bounceSound;


	// Use this for initialization
	void Start () {

		rb = GetComponent <Rigidbody2D> ();
		
	}

	void Update (){

		if (transform.position.y < -4) {

			GameObject level_manager = GameObject.FindGameObjectWithTag ("Level Manager");

			level_manager.SendMessage ("Death");

		}
	}

	void FixedUpdate () {

		transform.Translate (new Vector3 (moveSpeed, 0, 0) * Time.deltaTime);

	}


	void OnCollisionEnter2D (Collision2D colWall) {

		if (colWall.gameObject.tag == "Wall") {

			transform.Rotate (0, 180, 0);

			GetComponent<AudioSource> ().PlayOneShot (bounceSound);

		}
	}

	void OnCollisionStay2D (Collision2D colGround) {

		if (colGround.gameObject.tag == "Platform") {

			rb.AddForce (Vector2.up * jump, ForceMode2D.Impulse);

			GetComponent<AudioSource> ().PlayOneShot (bounceSound);

		}

	}
}

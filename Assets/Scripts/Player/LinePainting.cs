﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinePainting : MonoBehaviour {

	private LineRenderer paint;
	private bool isLeftMousePressed;
	private bool isRightMousePressed;
	private bool isMiddleMousePressed;
	public List<Vector3> pointsList;
	private Vector3 mousePos;

	private Vector3 startPoint;
	private Vector3 endPoint;


	void Awake ()
	{
		paint = gameObject.AddComponent<LineRenderer> ();
		paint.material = new Material (Shader.Find ("Particles/Additive"));
		paint.SetVertexCount (0);
		paint.SetWidth (0.2f, 0.2f);
		paint.SetColors (Color.black, Color.black);
		paint.useWorldSpace = true;    
		isLeftMousePressed = false;
		isRightMousePressed = false;
		isMiddleMousePressed = false;
		pointsList = new List<Vector3> ();

	}
		
	void Update ()
	{

		if (Input.GetMouseButtonDown (0)) {
			isLeftMousePressed = true;
			paint.SetVertexCount (0);
			pointsList.RemoveRange (0, pointsList.Count);
			paint.SetColors (Color.blue, Color.blue);
		}
		if (Input.GetMouseButtonUp (0)) {
			isLeftMousePressed = false;
		}

		if (isLeftMousePressed) {
			mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePos.z = 0;
			if (!pointsList.Contains (mousePos)) {
				pointsList.Add (mousePos);
				paint.SetVertexCount (pointsList.Count);
				paint.SetPosition (pointsList.Count - 1, (Vector3)pointsList [pointsList.Count - 1]);
				startPoint = (Vector3)pointsList [pointsList.Count - 2];
				endPoint = (Vector3)pointsList [pointsList.Count - 1];
				addPlatformColliderToLine ();
			
			}
		}

		if (Input.GetMouseButtonDown (1)) {
			isRightMousePressed = true;
			paint.SetVertexCount (0);
			pointsList.RemoveRange (0, pointsList.Count);
			paint.SetColors (Color.red, Color.red);
		}
		if (Input.GetMouseButtonUp (1)) {
			isRightMousePressed = false;
		}

		if (isRightMousePressed) {
			mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePos.z = 0;
			if (!pointsList.Contains (mousePos)) {
				pointsList.Add (mousePos);
				paint.SetVertexCount (pointsList.Count);
				paint.SetPosition (pointsList.Count - 1, (Vector3)pointsList [pointsList.Count - 1]);
				startPoint = (Vector3)pointsList [pointsList.Count - 2];
				endPoint = (Vector3)pointsList [pointsList.Count - 1];
				addWallColliderToLine ();

			}
		}

		if (Input.GetMouseButtonDown (2)) {
			
			isMiddleMousePressed = true;
			paint.SetVertexCount (0);
			pointsList.RemoveRange (0, pointsList.Count);
			paint.SetColors (Color.green, Color.green);

		}

		if (Input.GetMouseButtonUp (2)) {
			
			isMiddleMousePressed = false;

		}

		if (isMiddleMousePressed) {
			
			mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePos.z = 0;

			if (!pointsList.Contains (mousePos)) {
				
				pointsList.Add (mousePos);
				paint.SetVertexCount (pointsList.Count);
				paint.SetPosition (pointsList.Count - 1, (Vector3)pointsList [pointsList.Count - 1]);
				startPoint = (Vector3)pointsList [pointsList.Count - 2];
				endPoint = (Vector3)pointsList [pointsList.Count - 1];
				addStickyColliderToLine ();

			}
		}
	}


	private void addPlatformColliderToLine () {
		
		BoxCollider2D col = new GameObject("Collider").AddComponent<BoxCollider2D> ();
		col.tag = "Platform";
		col.transform.parent = paint.transform;
		float lineLength = Vector3.Distance (startPoint, endPoint);
		col.size = new Vector3 (lineLength, 0.1f, 1f);
		Vector3 midPoint = (startPoint + endPoint)/2;
		col.transform.position = midPoint;
		float angle = (Mathf.Abs (startPoint.y - endPoint.y) / Mathf.Abs (startPoint.x - endPoint.x));

		if ((startPoint.y<endPoint.y && startPoint.x>endPoint.x) || (endPoint.y<startPoint.y && endPoint.x>startPoint.x)) {
			
			angle*=-1;

		}

		angle = Mathf.Rad2Deg * Mathf.Atan (angle);
		col.transform.Rotate (0, 0, angle);

	}


	private void addWallColliderToLine()
	{
		BoxCollider2D col = new GameObject("Collider").AddComponent<BoxCollider2D> ();
		col.tag = "Wall";
		col.transform.parent = paint.transform;
		float lineLength = Vector3.Distance (startPoint, endPoint);
		col.size = new Vector3 (lineLength, 0.1f, 1f);
		Vector3 midPoint = (startPoint + endPoint)/2;
		col.transform.position = midPoint;
		float angle = (Mathf.Abs (startPoint.y - endPoint.y) / Mathf.Abs (startPoint.x - endPoint.x));
		if((startPoint.y<endPoint.y && startPoint.x>endPoint.x) || (endPoint.y<startPoint.y && endPoint.x>startPoint.x))
		{
			angle*=-1;
		}
		angle = Mathf.Rad2Deg * Mathf.Atan (angle);
		col.transform.Rotate (0, 0, angle);
	}

	private void addStickyColliderToLine()
	{
		BoxCollider2D col = new GameObject("Collider").AddComponent<BoxCollider2D> ();
		col.transform.parent = paint.transform; // Collider is added as child object of line
		float lineLength = Vector3.Distance (startPoint, endPoint); // length of line
		col.size = new Vector3 (lineLength, 0.1f, 1f); // size of collider is set where X is length of line, Y is width of line, Z will be set as per requirement
		Vector3 midPoint = (startPoint + endPoint)/2;
		col.transform.position = midPoint; // setting position of collider object
		// Following lines calculate the angle between startPos and endPos
		float angle = (Mathf.Abs (startPoint.y - endPoint.y) / Mathf.Abs (startPoint.x - endPoint.x));
		if((startPoint.y<endPoint.y && startPoint.x>endPoint.x) || (endPoint.y<startPoint.y && endPoint.x>startPoint.x))
		{
			angle*=-1;
		}
		angle = Mathf.Rad2Deg * Mathf.Atan (angle);
		col.transform.Rotate (0, 0, angle);
	}
}
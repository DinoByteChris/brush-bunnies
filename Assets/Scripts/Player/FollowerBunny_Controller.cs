﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerBunny_Controller : MonoBehaviour {

	public string follower = "Follower";
	public int maxFollowers = 3;
	public float followDamping = 5;
	public float followDistance = 0.5f;

	public int bunniesCollected;

	private List<GameObject> collected;
	private List<SpringJoint2D> joints;

	public float jump;

	public Transform leader;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () {

		rb = GetComponent <Rigidbody2D> ();

		collected = new List<GameObject> ();

		joints = new List <SpringJoint2D> ();
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.LookAt (leader);

		if (Input.GetKeyDown (KeyCode.R)) {
			RemoveAll ();
		}

	}

	void OnCollisionEnter2D (Collision2D colNPC) {

		if (colNPC.gameObject.CompareTag (follower)) {

			if (collected.Count >= maxFollowers)
				return;

			AddFollower (colNPC.gameObject);

		}
	}

	void OnCollisionStay2D (Collision2D colGround) {

		if (colGround.gameObject.tag == "Platform") {

			rb.AddForce (Vector2.up * jump, ForceMode2D.Impulse);

		}

	}

	void AddFollower (GameObject obj) {

		var j = gameObject.AddComponent<SpringJoint2D> ();

		j.autoConfigureConnectedAnchor = false;
		j.dampingRatio = followDamping;
		j.connectedBody = obj.GetComponent<Rigidbody2D> ();

		if (followDistance > 0) {

			j.autoConfigureDistance = false;
			j.distance = followDistance;
		}

		joints.Add(j);

		collected.Add (obj);

	}

	void RemoveAll() {

		foreach (var joint in joints) {

			Destroy (joint);

		}

		collected.Clear ();

	}

}

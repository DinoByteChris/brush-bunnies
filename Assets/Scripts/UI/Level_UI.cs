﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level_UI : MonoBehaviour {

	public Text livesLeft;
	public Text bunniesCollected;
	public Text score;

	private GameObject level_manager;
	
	// Update is called once per frame
	void Update () {

		level_manager = GameObject.Find ("Level_Manager");

		Level_Manager lev_man = level_manager.GetComponent <Level_Manager> ();

		livesLeft.text = "Lives: " + lev_man.playerLives;

		bunniesCollected.text = "Bunnies: " + lev_man.followers + "/" + lev_man.totalCollectedRequired;

		score.text = "Score: " + lev_man.score;
		
	}
}

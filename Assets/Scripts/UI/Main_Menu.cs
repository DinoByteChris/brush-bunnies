﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Main_Menu : MonoBehaviour {

	public string levelToLoad;

	public Button uiButton;

	// Use this for initialization
	void Awake () {

		uiButton = GetComponent<Button> ();

	}
	
	public void ButtonFunction () {

		SceneManager.LoadScene (levelToLoad);

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour {

	public Transform leader;
	public float speed;
	public float xDifference;
	public float yDifference;
	public float movementThreshold;

	private Vector3 moveTemp;


	// Update is called once per frame
	void Update () {

		if (leader.transform.position.x > transform.position.x) {
			xDifference = leader.transform.position.x - transform.position.x;
		}
		else {
			xDifference = transform.position.x - leader.transform.position.x;	
		}

		if (leader.transform.position.y > transform.position.y) {
			yDifference = leader.transform.position.y - transform.position.y;
		}
		else {
			yDifference = transform.position.y - leader.transform.position.y;	
		}
			

		if (xDifference >= movementThreshold || yDifference >= movementThreshold) {

			moveTemp = leader.transform.position;
			moveTemp.z = -10;

			transform.position = Vector3.MoveTowards (transform.position, moveTemp, speed * Time.deltaTime);

		}	
	}
}
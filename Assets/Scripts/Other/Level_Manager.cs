﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Level_Manager : MonoBehaviour {

	public int totalCollectedRequired;
	public int score;
	public string levelToLoad;

	public AudioClip followerCollectSound;
	public AudioClip deathSound;
	public AudioClip levelCompleteSound;

	[HideInInspector]
	public int playerLives;
	[HideInInspector]
	public int followers;


	void Awake () {

		playerLives = 3;

		followers = 0;

		score = 0;

	}
	

	void Update () {

		GameOver ();

	}

	void LevelProgress (int bunniesCollected) {

		followers += bunniesCollected;

		GetComponent<AudioSource> ().PlayOneShot (followerCollectSound);
	
	}

	void Death () {

		playerLives--;

		GetComponent<AudioSource> ().PlayOneShot (deathSound);

	}


	void GameOver () {

		if (playerLives == 0) {

			SceneManager.LoadScene ("Test_Menu");

		}
	}


}
